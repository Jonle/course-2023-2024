1) 
MATCH (n) - [:type]-(:Airline) return count(n)

2) 
MATCH (n) - [:type]-(:Aircraft) return count(n)
MATCH (n) - [:type]-(:Aircraft) where n.hasStatus="Active" return count(n)
MATCH (n) - [:type]-(:Aircraft) where n.hasStatus="Inactive" return count(n)

3) 
match (n)-[:type]-(:Airline), (n)-[:Owns]-(p)-[:type]-(:Aircraft) where p.hasStatus="Active"  return n, count(p)

4) 
match (n)-[:type]-(:Aircraft) where n.hasCreationDate<"2010" return count(n)
# this works because it stringcompares the start of the string

5)
match (n)-[:type]-(:Aircraft) where n.hasModificationDate is not null return count(n)
match (n)-[:type]-(:Aircraft) with 
    datetime(replace(n.hasModificationDate,"_","-")) as md,
    datetime(replace(n.hasCreationDate,"_","-")) as cd,
    n 
 where md.year = (cd.year + 1) return count(n)

6)
match (n)-[:type]-(:AircraftModel) return count(distinct n)

7)
match (n)-[:hasModelVersion]->(v) with n, count(distinct v) as mv where mv > 5 return n

8)
match (n)-[:hasModelVersion]->(v) with n, count(distinct v) as mv where mv > 5 return distinct n.isManufacturedBy

9)
match (n)-[:type]->(:AircraftModel) where n.isManufacturedBy is null return count(n)

10)
match (n)-[:type]->(class) unwind keys(n) as pk return distinct pk