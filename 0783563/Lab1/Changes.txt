Changes 11/10/2023
##### File: Oef1 #####
Changed xsd::uri to xsd:string

##### File: r0783563_OWL.ttl #####
I remade the exercise, since I only made 6 classes and 1 object property during the lab session. (So everything is new)


Changes 20/10/2023
##### File: Oef1 #####
Added rdf and rdfs prefixes instead of class prefix. 
Added line 8-10.
Fixed wrong subject for predicate "rating", "source" and "rdf:type class:Restaurant".
