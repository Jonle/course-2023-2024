prefixes:
  ex: "http://www.example.org/"
  Airport: "http://example.org/Airport/"
  ontology: "http://data.sfgov.org/ontology/"
  rr: "http://www.w3.org/ns/r2rml#"
  foaf: "http://xmlns.com/foaf/0.1/"
  xsd: "http://www.w3.org/2001/XMLSchema#"
  rdfs: "http://www.w3.org/2000/01/rdf-schema#"
  
mappings:
  Aircraft:
    sources:
        - [sfo-aircraft-tail-numbers-and-models.csv~csv]
    s:  ex:Aircraft/$(aircraft_id)
    po: 
        - [a, ontology:Aircraft]
        - [ontology:hasID, $(aircraft_id)]
        - [ontology:hasAircraftModel, $(aircraft_model)] 
        - [ontology:hasOwningAirline, $(airline)]  
        - [ontology:hasCreationDate, $(creation_date), xsd:dateTime]
        - [ontology:hasModificationDate, $(modification_date), xsd:dateTime]
        - [ontology:hasStatus, $(status), xsd:boolean] 
        - [ontology:hasTailNumber, $(tail_number), xsd:Literal]

  Airline:
    sources:
        - [sfo-aircraft-tail-numbers-and-models.csv~csv]
    s:  ex:Airline/$(airline)
    po:
        - [a, ontology:Airline]
        - [ontology:Owns, $(aircraft_id), xsd:integer] 

  AircraftModel:
    sources:
        - [sfo-air-traffic-landing-statistics.csv~csv]
    s:  ex:AircraftModel/$(aircraft_model)
    po:
        - [a, ontology:AircraftModel]
        - [ontology:hasModelName, $(aircraft_model), xsd:Literal]
        - [ontology:hasBodyType, $(aircraft_body_type), xsd:Literal]
        - [ontology:isManufacturedBy, $(aircraft_manufacturer), xsd:Literal]
        - [ontology:hasModelVersion, $(aircraft_version), xsd:Literal]

  AircraftModelVersion:
    sources:
        - [sfo-air-traffic-landing-statistics.csv~csv]
    s:  ex:AircraftModelVersion/$(aircraft_version)
    po:
      - [a, ontology:AircraftModelVersion]
      - [ontology:hasModelVersion, $(aircraft_version), xsd:Literal]