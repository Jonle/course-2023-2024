Sandbox Queries (task a):

PREFIX P: <http://GP-onto.fi.upm.es/exercise2#>

1. SELECT ?s WHERE {?s a rdfs:Class}
2. SELECT ?s WHERE {?s rdfs:subClassOf P:Establishment}
3. SELECT ?s WHERE {?s a P:City}
4. SELECT ?o WHERE {P:Santiago_de_Compostela P:hasInhabitantNumber ?o}
5. SELECT ?o WHERE {{P:Santiago_de_Compostela P:hasInhabitantNumber ?o} UNION {P:Arzua P:hasInhabitantNumber ?o}}
6. SELECT DISTINCT ?s ?o WHERE {?s P:hasInhabitantNumber ?o} ORDER BY ?s
7. SELECT DISTINCT ?s ?o WHERE {?sub rdfs:subClassOf P:Locality. ?s a ?sub. OPTIONAL { ?s P:hasInhabitantNumber ?o}}
8. SELECT ?s ?o WHERE {?s P:hasInhabitantNumber ?o. FILTER(xsd:integer(?o) > 200000).}
9. DESCRIBE ?o WHERE {P:Pazo_Breogan P:hasAddress ?o}
10. SELECT ?s WHERE {?s rdfs:subClassOf P:Location}
11. SELECT ?s WHERE {?sub rdfs:subClassOf P:Locality. ?s a ?sub}
12. DESCRIBE ?s WHERE {?s rdfs:label "Madrid"}
13. CONSTRUCT {?s P:IsIn ?o} WHERE {?sub rdfs:subClassOf P:TouristicLocation. ?s a ?sub. ?s P:isPlacedIn ?loc. ?loc P:inProvince ?o.}
14. ASK WHERE {P:Town ?p ?o}

GraphDB Queries (task b):

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ontology: <http://data.sfgov.org/ontology/>

1. select distinct ?o where {?s a ?o}
2. select distinct ?p where {?s a ?o. ?s ?p ?x.}
3. select distinct ?p where {?s a ?o. ?s ?p ?x. filter (?p != rdf:type).}
ontology: Aircraft
4. select (count(distinct ?o) as ?count) where {?s a ontology:Aircraft. ?s ?p ?o. filter (?p != rdf:type).}
5. select distinct ?p (count(distinct ?o) as ?count) where {?s a ontology:Aircraft. ?s ?p ?o. filter (?p != rdf:type).} group by ?p
6. select distinct ?p (avg(?count) as ?average) where {select distinct ?p (count(distinct ?o) as ?count) where {?s a ontology:Aircraft. ?s ?p ?o. filter (?p != rdf:type).} group by ?p} group by ?p
7. select distinct ?p (count(distinct ?o) as ?count) where {?s a ontology:Aircraft. ?s ?p ?o. filter (?p != rdf:type).} group by ?p order by desc(?count)