@base <http://example.org/> .
@prefix rel: <http://example.org/rel/> .
@prefix class: <http://example.org/class/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<La_Mamounia> rel:type class:Hotel .
<La_Mamounia> rel:type class:Restaurant .
<La_Mamounia> rel:located_in <Marrakech> .
<La_Mamounia> rel:featured_in <Conde_Nast_Traveler> .
<La_Mamounia> rel:rating "4.5" .
<La_Mamounia> rel:reservation_link "https://mamounia.com/fr .
<La_Mamounia> rel:source "https://www.cntraveller.com/article/best-hotels-marrakech-morocco" .

<Conde_Nast_Traveler> rel:URL "https://www.cntraveler/com/" .

<Cuisine_De_Terroir> rel:located_in <Marrakech> .
