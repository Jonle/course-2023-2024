task A:

1) select ?class where {?class a rdfs:Class} 

2) PREFIX p: <http://GP-onto.fi.upm.es/exercise2#>
   SELECT ?subclass WHERE {?subclass rdfs:subClassOf p:Establishment}

3)SELECT ?instance WHERE {?instance rdf:type p:City}

4)SELECT ?people WHERE {p:Santiago_de_Compostela p:hasInhabitantNumber ?people}

5)
SELECT ?people 
WHERE {
{p:Arzua p:hasInhabitantNumber ?people} 
UNION 
{p:Santiago_de_Compostela p:hasInhabitantNumber ?people}
}:

6)
SELECT DISTINCT ?place ?people 
WHERE {
?place p:hasInhabitantNumber ?people
} 
ORDER BY ?place

7)
SELECT DISTINCT ?subclass ?people
WHERE {
?sub rdfs:subClassOf p:Locality.
?subclass a ?sub.
OPTIONAL{?subclass p:hasInhabitantNumber ?people}}

8)
select ?place where {
?place p:hasInhabitantNumber ?aantal
FILTER(xsd:integer(?aantal) > 200000)
} 

9)
PREFIX p: <http://GP-onto.fi.upm.es/exercise2#>
DESCRIBE ?info where {
p:Pazo_Breogan p:hasAddress ?info
} 

10)
SELECT ?subclass
WHERE {
  ?subclass rdfs:subClassOf* p:Location
}

11)
SELECT ?instance
WHERE {
  ?instance ?p p:Locality
}

12)
DESCRIBE ?resource
WHERE {
  ?resource rdfs:label "Madrid"
}

13)
PREFIX a: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
CONSTRUCT {?place p:isIn ?province.}
WHERE
{
?touristicPlace    rdfs:subClassOf p:TouristicLocation .
?place             a:type          ?touristicPlace.
?place             p:isPlacedIn    ?provinceplace .     
?provinceplace     p:inProvince    ?province .
}

14)
ASK
WHERE {
  ?town a:type p:Town .
}
