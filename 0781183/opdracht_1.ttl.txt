@base <http://example.org/> .
@prefix class: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rel: <https://www.preceive.net/schemas/relationship/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<Cuisine_De_Terroir>	rel:rating	xsd:4.5 .
<Cuisine_De_Terroir>	rel:typeOf	class:Restaurant .
<Cuisine_De_Terroir>	rel:LocatedIn	<Marrakech> .

<Marrakech>

<La_Mamounia>	rel:typeOf	class:Hotel .
<La_Mamounia>	rel:featured_in	<Conde_Nast_Traveler> .